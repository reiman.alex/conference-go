import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}+{state}"

    header = {
        "Authorization": PEXELS_API_KEY,
    }

    response = requests.get(url, headers=header)
    return response.json()["photos"][0]["src"]["medium"]


def get_coord(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=5&appid={OPEN_WEATHER_API_KEY}"
    resp = requests.get(url)
    lat = resp.json()[0]["lat"]
    lon = resp.json()[0]["lon"]
    return {
        "lat": lat,
        "lon": lon,
    }


def get_weather(lat, lon):
    url = f"http://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    resp = requests.get(url)
    return {
        "description": resp.json()["weather"][0]["description"],
        "temp": resp.json()["main"]["temp"],
    }
